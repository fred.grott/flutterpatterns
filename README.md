![android](/media/small-screenshot-2019-12-01_08.40.48.395.png)![ios](/media/small-screenshot-2019-12-01_08.44.49.684.png)

# Videos

[! Android Light and dark ](/media/mq2.jpg)](
https://www.youtube.com/watch?v=uwXid0AqV2I)

Note, to get pixel perfect 1-to-1 scaling, theme-ing consitency, etc takes about 25 classes even with the flutter_platform_wgdets plugin. But by doing this now upfront I can offer pixel perfection to every client. hence, the boilerplate app project ot owokr those things out, fully tested.

[flutter_boilerplagte_app on gitlab](https://gitlab.com/fred.grott/flutter_boilerplate_app)

# FlutterPatterns

Fredrick Allan Grott(Fred Grott) Flutter Mobile Development
Location: Greater Chicago Area(NW Indiana, CST GMT -6)

## The Value Pitch

Typical 4.5 months ios and android apps that would have normally cost a total of 450,000 US Dollars now at 200,000 US Dollars because unlike the fake promise of FB React Native it really is just one code base and one UI tree.

From someone that knows the Flutter internals and is working on a community effort via the Flutter_Platform_widgets to polish the material widgets to android with material look and cupertino widgets to ios with Apple's flat design look approach to mobile design.

I do not sub-contract this work, its all mine and controlled by myself to reach the highet quality possible, ie pixel perfection.

## What You Get As Standard

1. Correct Orientation Handling(both ios and android)
2. Responsive layouts Scaling(basic and also wokring on a premium version that gets as close to 1:1 scaing as possilbe)
3. Material Design Standard Pixeel Perfect on Android and 
pixelperfect Apple Flat UI design on ios  by default.
4. Able to fully test both visual ux and oop-code app dsign options with several UX versions for maximum feedback from the startup stakeholders and app users

## Credentials

### Flutter_Platform_Widgets

I joined this project in January 2020 and started contributing code. Its a community efffort to polish up Google's Theme apparoch to dynamically deliver material loook to android and Apple Flat Design look to ios.

https://github.com/aqwert/flutter_platform_widgets

You can see how popular it is in the Flutter Community here at this link(score of 98 including code QA):

https://pub.dev/packages/flutter_platform_widgets#-analysis-tab-


### Previous Android Native(java-kotlin)

Just reusing my oop an design skills to rock flutter. Since I already knwo the rendering system used and the oop Google moved toward its real easy for me to plot a well designed and engineered course towards app pixel perfect perfectionusing flutter.

# Contact

fred DOT grott AT gmail DOT com

## Social

AngelList
https://angel.co/fred-grott



DesignHacksATYoutube-NO-Ads(I put mydesign progrees here)
https://www.youtube.com/channel/UCBRREUKFNWIELQ3oBzeXiMA?view_as=subscriber

Gitlab
https://gitlab.com/fred.grott



Medium
https://medium.com/@fredgrott

Twitter
https://twitter.com/fredgrott


LinkedIN
https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/


